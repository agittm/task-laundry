﻿using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public class DeviceDataModel
{
    public bool IsMuted;

    public DeviceDataModel()
    {
        IsMuted = false;
    }

    public DeviceDataModel(JSONNode json)
    {
        IsMuted = json["is_muted"].AsBool;
    }

    public JSONObject ToJSON()
    {
        JSONObject json = new JSONObject();

        json["is_muted"].AsBool = IsMuted;

        return json;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneMenuManager : GameSceneManager
{
    [SerializeField] GameObject[] panels;

    public void ShowMenu(GameObject obj)
    {
        for (int i = 0; i < panels.Length; i++)
        {
            panels[i].SetActive(panels[i] == obj);
        }
    }
}
﻿using UnityEngine;

public class BackButton : BasicButton
{
    public override void Click()
    {
        base.Click();
    }

    public override void PlaySFX()
    {
        SoundManager.Instance.PlaySFX("button_back");
    }
}
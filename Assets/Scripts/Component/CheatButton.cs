﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheatButton : BasicButton
{
    private void Start()
    {
#if !USE_CHEAT
        Destroy(this);
#endif
    }

    public override void Click()
    {
        base.Click();
    }
}
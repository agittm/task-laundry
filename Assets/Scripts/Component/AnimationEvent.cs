﻿using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class UnityEventAnimation : UnityEvent<string>
{

}

public class AnimationEvent : MonoBehaviour
{
    public UnityEventAnimation OnFinished;

    public void FinishClip(string code)
    {
        OnFinished?.Invoke(code);
    }
}